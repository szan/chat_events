require 'auth/auth_token'

module Api
  class AuthController < ApplicationController
    skip_before_action :authenticate_user!

    def authenticate
      user = User.find_by(username: params[:username])&.authenticate(params[:password])
      if user
        render json: { auth_token: AuthToken.generate(user) }
      else
        render json: { error: 'Invalid username or password' }, status: :unauthorized
      end
    end
  end
end
