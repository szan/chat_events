class UsersController < ApplicationController
  def show
    render json: current_user.as_json(only: :username)
  end
end
