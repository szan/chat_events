require 'auth/jwt_utils'

class ApplicationController < ActionController::Base

  skip_before_action :verify_authenticity_token
  before_action :authenticate_user!

  def authenticate_user!
    fail Errors::AuthenticationTimeout if auth_token_expired?
    fail Errors::NotAuthenticated unless current_user
  end

  def current_user
    @current_user ||= User.find_by(id: decoded_auth_token[:user_id])
  end

  def auth_token_expired?
    decoded_auth_token&.expired?
  end

  def decoded_auth_token
    @decoded_auth_token ||= auth_token && JWTUtils.decode(auth_token)
  end

  def auth_token
    @auth_token ||= request.headers['Authorization'].presence&.split(' ')&.last
  end
end
