import Vue from 'vue'
import VueRouter from 'vue-router'
import SignIn from './components/SignIn'
import Chat from './components/Chat'

Vue.use(VueRouter)

const router = new VueRouter({
    // mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Sign In',
            component: SignIn
        },
        {
            path: '/chat',
            name: 'Chat',
            component: Chat
        }
    ]
})

export default router
