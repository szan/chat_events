class User < ApplicationRecord
  has_secure_password

  validates :username, presence: true, uniqueness: true, length: { minimum: 4 }
  validates :password, length: { minimum: 8 }, allow_nil: true
  before_validation :strip_values

  private

  def strip_values
    self.username = username&.strip&.downcase
  end
end
