Rails.application.routes.draw do
  root to: 'home#index'

  namespace :api do
    post 'auth' => 'auth#authenticate'

    resource :user, only: [:show]
  end
end
