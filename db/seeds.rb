# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Rails.application.config_for(:seed_users)['users'].each do |user|
  User.where(username: user['username']).first_or_create!(password: user['password']) do
    puts "user #{user['username']} created"
  end
end
