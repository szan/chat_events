require_relative 'jwt_utils'

class AuthToken

  def self.generate(user)
    new(user).encoded
  end

  def initialize(user)
    raise ArgumentError, 'You must provide User model to generate token' unless user
    @user = user
  end

  def payload
    { user_id: @user.id }
  end

  def encoded
    JWTUtils.encode(payload)
  end
end
