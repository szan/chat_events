class DecodedAuthToken < HashWithIndifferentAccess
  def expired?
    self[:iat] <= 24.hours.ago.to_i
  end
end
