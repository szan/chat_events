require_relative 'decoded_auth_token'

class JWTUtils
  ALGORITHM = 'HS256'

  def self.encode(payload)
    payload[:iat] = Time.now.to_i
    JWT.encode(payload, Rails.application.secrets.secret_key_base, ALGORITHM)
  end

  def self.decode(token)
    payload = JWT.decode(token, Rails.application.secrets.secret_key_base, true, { algorithm: ALGORITHM })[0]
    DecodedAuthToken.new(payload)
  rescue
    nil
  end
end
